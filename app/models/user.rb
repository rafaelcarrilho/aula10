
class User < ApplicationRecord
  enum kind: [
    :admin,
    :standard
  ]

  devise  :database_authenticatable, 
          :jwt_authenticatable,
          :registerable, 
          jwt_revocation_strategy: JwtBlacklist
end

